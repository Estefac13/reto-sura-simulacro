package com.sauce.stepdefinition.products;

import com.sauce.model.login.LoginModel;
import com.sauce.page.pages.login.LoginCases;
import com.sauce.page.pages.products.ProductCases;
import com.sauce.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

public class OrderProductsStepDefinition extends SetUp {

    private static final Logger LOGGER=Logger.getLogger(OrderProductsStepDefinition.class);
    private LoginModel loginModel;
    private ProductCases productCases;
    @Dado("que el usuario se logueo y se encuentra en la sección de productos")
    public void queElUsuarioSeLogueoYSeEncuentraEnLaSeccionDeProductos() {
        try{
            generalSetUp();
            loginModel= setUpLoginData();
            LoginCases loginCases = new LoginCases(driver, loginModel);
            loginCases.fillLogin();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Cuando("el usuario ordene los productos según el precio mayor al menor")
    public void elUsuarioOrdeneLosProductosSegunElPrecioMayorAlMenor() {
        try{
            productCases=new ProductCases(driver);
            productCases.orderHighToLow();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("el usuario podrá observar que se ordenaron correctamente los productos")
    public void elUsuarioPodraObservarQueSeOrdenaronCorrectamenteLosProductos() {
        try{
            Assertions.assertEquals(productCases.savePrices(),productCases.isOrderedPricesHighToLow());
            quitDriver();
        }catch(Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }
    }

    @Cuando("el usuario ordene los productos según el precio menor al mayor")
    public void elUsuarioOrdeneLosProductosSegunElPrecioMenorAlMayor() {
        try{
            productCases=new ProductCases(driver);
            productCases.orderLowToHigh();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("el usuario podrá observar que se ordenaron los productos")
    public void elUsuarioPodraObservarQueSeOrdenaronLosProductos() {
        try{
            Assertions.assertEquals(productCases.savePrices(),productCases.isOrderedPricesLowToHigh());
            quitDriver();
        }catch (Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }
    }

    @Cuando("el usuario ordene los productos según su nombre ascendentemente")
    public void elUsuarioOrdeneLosProductosSegunSuNombreAscendentemente() {
        try{
            productCases=new ProductCases(driver);
            productCases.orderNameAZ();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("el usuario podrá observar que se ordenaron los productos de forma ascendente")
    public void elUsuarioPodraObservarQueSeOrdenaronLosProductosDeFormaAscendente() {
        try{
            Assertions.assertEquals(productCases.saveNames(),productCases.isOrderedNameAZ());
            quitDriver();
        }catch (Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }
    }

    @Cuando("el usuario ordene los productos según su nombre descendentemente")
    public void elUsuarioOrdeneLosProductosSegunSuNombreDescendentemente() {
        try{
            productCases=new ProductCases(driver);
            productCases.orderNameZA();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("que el usuario podrá observar que se ordenaron los productos de forma descendente")
    public void queElUsuarioPodraObservarQueSeOrdenaronLosProductosDeFormaDescendente() {
        try{
            Assertions.assertEquals(productCases.saveNames(),productCases.isOrderedNameZA());
            quitDriver();
        }catch (Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }
    }



    public LoginModel setUpLoginData(){
        loginModel=new LoginModel();
        loginModel.setUser("standard_user");
        loginModel.setKey("secret_sauce");
        return this.loginModel;
    }
}
