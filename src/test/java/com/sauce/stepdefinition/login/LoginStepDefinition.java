package com.sauce.stepdefinition.login;

import com.sauce.model.login.LoginModel;
import com.sauce.page.pages.login.LoginCases;
import com.sauce.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

import static com.sauce.util.Messages.MESSAGE_LOGIN_ERROR;

public class LoginStepDefinition extends SetUp {

    private static final Logger LOGGER= Logger.getLogger(LoginStepDefinition.class);

    private LoginModel loginModel;

    private LoginCases loginPageCases;

    @Dado("que el usuario se encuentra en la sección de login")
    public void queElUsuarioSeEncuentraEnLaSeccionDeLogin() {
        try{
            generalSetUp();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }

    //Escenario 1
    @Cuando("el usuario suministre las credenciales de un usuario estandar correctamente")
    public void elUsuarioSuministreLasCredencialesDeUnUsuarioEstandarCorrectamente() {
        try{
            loginModel= setUpLoginData();
            loginPageCases=new LoginCases(driver,loginModel);
            loginPageCases.fillLogin();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("el usuario podrá observar que se logueo correctamente en la tienda")
    public void elUsuarioPodraObservarQueSeLogueoCorrectamenteEnLaTienda() {
        try{
            Assertions.assertEquals(true,loginPageCases.titleIsPresent());
            quitDriver();
        }catch (Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }

    }

    //Escenario 2
    @Cuando("el usuario suministre las credenciales de un usuario estandar incorrectamente")
    public void elUsuarioSuministreLasCredencialesDeUnUsuarioEstandarIncorrectamente() {
        try{
            loginModel= setUpIncorrectLoginData();
            loginPageCases=new LoginCases(driver,loginModel);
            loginPageCases.fillLogin();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }

    }
    @Entonces("el usuario podrá observar que ingreso mal los datos de ingreso al sitio web")
    public void elUsuarioPodraObservarQueIngresoMalLosDatosDeIngresoAlSitioWeb() {
        try{
            Assertions.assertEquals(infoExpectedLoginError(),loginPageCases.infoActualError());
            quitDriver();
        }catch (Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }

    }

    public LoginModel setUpLoginData(){
        loginModel=new LoginModel();
        loginModel.setUser("standard_user");
        loginModel.setKey("secret_sauce");
        return this.loginModel;
    }

    public LoginModel setUpIncorrectLoginData(){
        loginModel=new LoginModel();
        loginModel.setUser("standard_user");
        loginModel.setKey("standard_user");
        return loginModel;
    }

    public ArrayList<String> infoExpectedLoginError(){
        ArrayList<String> infoExpectedLoginError=new ArrayList<>();
        infoExpectedLoginError.add(MESSAGE_LOGIN_ERROR.getValue());
        return infoExpectedLoginError;
    }
}
