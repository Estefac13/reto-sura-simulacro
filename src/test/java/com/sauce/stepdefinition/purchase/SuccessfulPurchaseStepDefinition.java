package com.sauce.stepdefinition.purchase;

import com.sauce.model.login.LoginModel;
import com.sauce.model.purchase.PurchaseModel;
import com.sauce.page.pages.login.LoginCases;
import com.sauce.page.pages.purchase.PurchaseCases;
import com.sauce.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

import static com.sauce.data.purchase.PurchaseData.purchaseModelData;
import static com.sauce.util.Messages.MESSAGE_CONFIRMATION_ORDER;

public class SuccessfulPurchaseStepDefinition extends SetUp {

    private static final Logger LOGGER=Logger.getLogger(SuccessfulPurchaseStepDefinition.class);
    private LoginModel loginModel;

    private LoginCases loginCases;

    private PurchaseModel purchaseModel;

    private PurchaseCases purchaseCases;
    @Dado("que el usuario se logueo")
    public void queElUsuarioSeLogueo() {
        try{
            generalSetUp();
            loginModel= setUpLoginData();
            loginCases=new LoginCases(driver,loginModel);
            loginCases.fillLogin();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Cuando("agregue productos al carro y confirme la compra")
    public void agregueProductosAlCarroYConfirmeLaCompra() {
        try{
            purchaseModel=purchaseModelData();
            purchaseCases=new PurchaseCases(driver,purchaseModel);
            purchaseCases.doPurchase();
        }catch(Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }
    @Entonces("el usuario podrá observar que se realizó correctamente la compra")
    public void elUsuarioPodraObservarQueSeRealizoCorrectamenteLaCompra() {
        try{
            Assertions.assertEquals(infoExpectedPurchase(),purchaseCases.infoActual());
            quitDriver();
        }catch(Exception e){
            quitDriver();
            Assertions.fail(e.getMessage(),e);
            LOGGER.error(e.getMessage());
        }
    }

    public LoginModel setUpLoginData(){
        loginModel=new LoginModel();
        loginModel.setUser("standard_user");
        loginModel.setKey("secret_sauce");
        return this.loginModel;
    }

    private ArrayList<String> infoExpectedPurchase(){
        ArrayList<String> infoExpected=new ArrayList<>();
        infoExpected.add(MESSAGE_CONFIRMATION_ORDER.getValue());
        return infoExpected;
    }

}
