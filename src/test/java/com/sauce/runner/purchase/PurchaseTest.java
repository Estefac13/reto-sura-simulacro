package com.sauce.runner.purchase;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        features = "src/test/resources/features/purchase/SuccessfulPurchase.feature",
        glue="com.sauce.stepdefinition.purchase"
)
public class PurchaseTest {
}
