package com.sauce.runner.login;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        features = "src/test/resources/features/login/LoginFeature.feature",
        glue="com.sauce.stepdefinition.login"
)
public class LoginTest {
}
