package com.sauce.setup;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static com.google.common.base.StandardSystemProperty.USER_DIR;
import static com.sauce.util.Log4j2Values.LOG4J_PROPERTIES_FILE_PATH;


public class SetUp {

    protected WebDriver driver;

    private static final String WEBDRIVER_CHROME_DRIVER="webdriver.chrome.driver";

    private static final String WEBDRIVER_CHROME_DRIVER_PATH="src/test/resources/webdriver/windows/chrome/chromedriver.exe";

    private static final String URL="https://www.saucedemo.com/";

    private static final Logger LOGGER= Logger.getLogger(SetUp.class);

    protected void generalSetUp(){
        try{
            setUpLog4j2();
            setUpDriver();
        }catch(Exception e){
            LOGGER.error(e.getMessage());
        }
    }

    private void setUpDriver(){
        try{
            System.setProperty(WEBDRIVER_CHROME_DRIVER,WEBDRIVER_CHROME_DRIVER_PATH);
            driver=new ChromeDriver();
            driver.get(URL);
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
        }catch (Exception e){
            quitDriver();
            LOGGER.error(e.getMessage());
        }
    }

    private void setUpLog4j2(){
        PropertyConfigurator.configure(USER_DIR.value()+LOG4J_PROPERTIES_FILE_PATH.getValue());
    }

    protected void quitDriver(){
        driver.quit();
    }

}
