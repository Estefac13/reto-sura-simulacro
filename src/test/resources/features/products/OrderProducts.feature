#language: es
  Característica:Ordenar los productos de precio mayor a menor
    Como usuario estandar de la tienda
    Necesito ordenar los productos
    Para filtrarlos según un parámetro

    Antecedentes:
      Dado que el usuario se logueo y se encuentra en la sección de productos

    Escenario: Ordenar los productos de precio mayor a menor de forma exitosa
      Cuando el usuario ordene los productos según el precio mayor al menor
      Entonces el usuario podrá observar que se ordenaron correctamente los productos


    Escenario: Ordenar los productos de precio menor a mayor de forma exitosa
      Cuando el usuario ordene los productos según el precio menor al mayor
      Entonces el usuario podrá observar que se ordenaron los productos

    Escenario: Ordenar los productos por el parametro de nombre de forma ascendente
      Cuando el usuario ordene los productos según su nombre ascendentemente
      Entonces el usuario podrá observar que se ordenaron los productos de forma ascendente

    Escenario: Ordenar los productos por el parámetro de nombre de forma descendente
      Cuando el usuario ordene los productos según su nombre descendentemente
      Entonces que el usuario podrá observar que se ordenaron los productos de forma descendente


