#language: es

  Característica: Login con usuario estandar en la tienda
    Como usuario estandar de la tienda
    Necesito loguearme en el sitio web de la tienda
    Para realizar una compra

    Escenario: Login exitoso
      Dado que el usuario se encuentra en la sección de login
      Cuando el usuario suministre las credenciales de un usuario estandar correctamente
      Entonces el usuario podrá observar que se logueo correctamente en la tienda


    Escenario: Login fallido
      Dado que el usuario se encuentra en la sección de login
      Cuando el usuario suministre las credenciales de un usuario estandar incorrectamente
      Entonces el usuario podrá observar que ingreso mal los datos de ingreso al sitio web