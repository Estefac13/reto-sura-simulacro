#language: es

  Característica: Compra de productos en la tienda
    Como usuario estandar de la tienda
    Necesito comprar varios productos en el sitio web
    Para satisfacer una necesidad personal

    Escenario: Compra exitosa
      Dado que el usuario se logueo
      Cuando agregue productos al carro y confirme la compra
      Entonces el usuario podrá observar que se realizó correctamente la compra