package com.sauce.util;

public enum Log4j2Values {
    LOG4J_PROPERTIES_FILE_PATH("\\src\\main\\resources\\log4j2.properties");
    private final String value;

    public String getValue(){
        return value;
    }
    Log4j2Values(String value){
        this.value=value;
    }

}
