package com.sauce.util;

public enum Messages {

    MESSAGE_LOGIN_ERROR("Epic sadface: Username and password do not match any user in this service"),
    MESSAGE_CONFIRMATION_ORDER("THANK YOU FOR YOUR ORDER");
    private String value;

    public String getValue(){
        return value;
    }
    Messages(String value){
        this.value=value;
    }
}
