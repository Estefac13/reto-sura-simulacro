package com.sauce.data.purchase;

import com.github.javafaker.Faker;
import com.sauce.model.purchase.PurchaseModel;

import java.util.Locale;

public class PurchaseData extends PurchaseModel {

    public static PurchaseModel purchaseModelData(){
        Faker faker=new Faker(new Locale("en-EN"));
        PurchaseModel purchaseModel=new PurchaseModel();
        purchaseModel.setName(faker.name().firstName());
        purchaseModel.setLastName(faker.name().lastName());
        purchaseModel.setPostalCode(faker.number().digits(4));
        return purchaseModel;
    }
}
