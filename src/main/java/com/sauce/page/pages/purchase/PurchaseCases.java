package com.sauce.page.pages.purchase;

import com.sauce.model.purchase.PurchaseModel;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class PurchaseCases extends PurchaseLocator{

    private final PurchaseModel purchaseModel;

    public PurchaseCases(WebDriver driver, PurchaseModel purchaseModel) {
        super(driver);
        this.purchaseModel=purchaseModel;
    }

    public void doPurchase(){
        clickRandomItem(btnAddToCartLocator);
        scrollOn(btnCart);
        clickOn(btnCart);
        scrollOn(btnCheckout);
        clickOn(btnCheckout);
        clearOn(firstNameLocator);
        typeOn(firstNameLocator,purchaseModel.getName());
        clearOn(lastNameLocator);
        typeOn(lastNameLocator,purchaseModel.getLastName());
        clearOn(postalCodeLocator);
        typeOn(postalCodeLocator,purchaseModel.getPostalCode());
        clickOn(btnContinueLocator);
        clickOn(btnFinish);
    }

    public List<String> infoActual(){
        ArrayList<String> infoActual=new ArrayList<>();
        infoActual.add(getText(confirmation).trim());
        return infoActual;
    }
}
