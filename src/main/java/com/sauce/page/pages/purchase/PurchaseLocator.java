package com.sauce.page.pages.purchase;

import com.sauce.page.common.CommonActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PurchaseLocator extends CommonActions {
    public PurchaseLocator(WebDriver driver) {
        super(driver);
    }

    protected By btnAddToCartLocator=By.cssSelector("button[class='btn btn_primary btn_small btn_inventory']");

    protected By btnCart=By.cssSelector("a[class='shopping_cart_link']");

    protected By btnCheckout=By.id("checkout");

    protected By firstNameLocator=By.id("first-name");

    protected By lastNameLocator=By.id("last-name");

    protected By postalCodeLocator=By.id("postal-code");

    protected By btnContinueLocator=By.id("continue");

    protected By btnFinish=By.id("finish");

    protected By confirmation=By.cssSelector("h2[class='complete-header']");

}
