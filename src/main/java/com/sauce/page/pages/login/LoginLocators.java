package com.sauce.page.pages.login;

import com.sauce.page.common.CommonActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginLocators extends CommonActions {

    protected final By userLocator=By.id("user-name");

    protected final By keyLocator=By.id("password");

    protected final By btnSubmitLocator=By.id("login-button");

    protected final By titleLocator=By.cssSelector("span[class='title']");

    protected final By errorMessageLocator=By.cssSelector("div[class='error-message-container error']");

    public LoginLocators(WebDriver driver) {
        super(driver);
    }
}
