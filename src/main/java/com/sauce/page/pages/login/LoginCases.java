package com.sauce.page.pages.login;

import com.sauce.model.login.LoginModel;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;


public class LoginCases extends LoginLocators {

    private final LoginModel loginModel;

    public LoginCases(WebDriver driver, LoginModel loginModel) {
        super(driver);
        this.loginModel=loginModel;
    }

    public void fillLogin(){
        scrollOn(userLocator);
        clickOn(userLocator);
        clearOn(userLocator);
        typeOn(userLocator,loginModel.getUser());
        scrollOn(keyLocator);
        clickOn(keyLocator);
        clearOn(keyLocator);
        typeOn(keyLocator,loginModel.getKey());
        scrollOn(btnSubmitLocator);
        clickOn(btnSubmitLocator);
    }

    public Boolean titleIsPresent(){
        return isPresent(titleLocator);
    }

    public List<String> infoActualError(){
        ArrayList<String> info=new ArrayList<>();
        info.add(getText(errorMessageLocator).trim());
        return info;
    }
}
