package com.sauce.page.pages.products;

import com.sauce.page.common.CommonActions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductLocator extends CommonActions {

    public ProductLocator(WebDriver driver) {
        super(driver);
    }

    protected By filterLocator=By.className("product_sort_container");
    protected By pricesLocator=By.cssSelector("div[class='inventory_item_price']");

    protected By namesLocator=By.cssSelector("div[class='inventory_item_name']");

}
