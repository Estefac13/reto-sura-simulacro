package com.sauce.page.pages.products;

import io.cucumber.java.bs.A;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProductCases extends ProductLocator{

    public ProductCases(WebDriver driver) {
        super(driver);
    }

    public void orderHighToLow(){
        selectItemByValue(filterLocator,"hilo");
    }

    public void orderLowToHigh(){selectItemByValue(filterLocator,"lohi");}

    public void orderNameAZ(){
        selectItemByValue(filterLocator,"az");
    }

    public void orderNameZA(){
        selectItemByValue(filterLocator,"za");
    }

    public List<Double> savePrices(){
        ArrayList<Double> precios=new ArrayList<>();
        List<WebElement> webElementList=saveItems(pricesLocator);
        for (WebElement x:webElementList){
            precios.add(Double.parseDouble(x.getText().replace("$","")));
        }
        return precios;
    }

    public List<String> saveNames(){
        ArrayList<String> nombres=new ArrayList<>();
        List<WebElement> webElementList=saveItems(namesLocator);

        for(WebElement x: webElementList){
            nombres.add(x.getText().trim());
        }
        return nombres;
    }

    public List<Double> isOrderedPricesHighToLow(){
        List<Double> precios=savePrices();
        precios.sort(Collections.reverseOrder());
        return precios;
    }

    public List<Double> isOrderedPricesLowToHigh(){
        List<Double> precios =savePrices();
        Collections.sort(precios);
        return precios;
    }

    public List<String> isOrderedNameAZ(){
        List<String> names=saveNames();
        Collections.sort(names);
        return names;
    }

    public List<String> isOrderedNameZA(){
        List<String> names=saveNames();
        names.sort(Collections.reverseOrder());
        return names;
    }
}
