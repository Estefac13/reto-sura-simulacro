package com.sauce.page.common;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CommonActions {

    private final WebDriver driver;

    public CommonActions(WebDriver driver) {
        this.driver = driver;
    }

    protected void scrollOn(By by){
        WebElement webElement=driver.findElement(by);
        JavascriptExecutor jse= (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].scrollIntoView();",webElement);
    }

    protected void clearOn(By by){
        WebElement webElement=driver.findElement(by);
        webElement.clear();
    }

    protected void clearOnJSE(By by){
        WebElement webElement=driver.findElement(by);
        JavascriptExecutor jse= (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].value='';",webElement);
    }

    protected void typeOn(By by,CharSequence... charSequences){
        WebElement webElement=driver.findElement(by);
        webElement.sendKeys(charSequences);
    }

    protected void typeOnJSE(WebElement webElement,String sequence){
        JavascriptExecutor jse=(JavascriptExecutor)driver;
        jse.executeScript("arguments[0].value='" + sequence +"';",webElement);
    }

    protected void clickOn(By by){
        WebElement webElement=driver.findElement(by);
        webElement.click();
    }

    protected void clickOnJSE(WebElement webElement){
        JavascriptExecutor jse=(JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();",webElement);
    }

    protected void clickOnJSE(By locator){
        JavascriptExecutor jse=(JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();",driver.findElement(locator));
    }

    protected void selectItemByIndex(By locator, int index){
        WebElement webElement=driver.findElement(locator);
        Select sel= new Select(webElement);
        sel.selectByIndex(index);
    }

    protected void selectItemByValue(By locator, String value){
        WebElement webElement=driver.findElement(locator);
        Select sel= new Select(webElement);
        sel.selectByValue(value);
    }

    protected void selectItemByVisibleText(By locator, String value){
        WebElement webElement=driver.findElement(locator);
        Select sel= new Select(webElement);
        sel.selectByVisibleText(value);
    }



    protected void explicitWait(By locator){
        WebDriverWait wdw=new WebDriverWait(driver,300);
        wdw.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
    }

    protected Boolean isPresent(By locator){
        WebElement webElement=driver.findElement(locator);
        return webElement.isDisplayed();
    }

    protected String getText(By locator){
        return driver.findElement(locator).getText();
    }

    protected void clickRandomItem(By locator){
        List<WebElement> webElementList=new ArrayList<>();
        webElementList=driver.findElements(locator);
        int number=webElementList.size();
        Random random=new Random();
        scrollOn(locator);
        webElementList.get(random.nextInt(number)).click();

    }

    protected List<WebElement> saveItems(By locator){
        List<WebElement> webElementList;
        webElementList=driver.findElements(locator);
        return webElementList;
    }
}
