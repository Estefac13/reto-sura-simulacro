package com.sauce.model.purchase;

public class PurchaseModel {

    private String name;
    private String lastName;
    private CharSequence postalCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CharSequence getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(CharSequence postalCode) {
        this.postalCode = postalCode;
    }
}
